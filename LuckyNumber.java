/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javaapplication3;

import java.util.ArrayList;
import static java.util.Collections.min;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

/**
 *
 * @author ngocb
 */
public class LuckyNumber {

    /**
     * @param args the command line arguments
     */
    int gues;
    int game=1;
    List<Integer> best = new ArrayList<>();
    
   
    public void play(){
        Random rd = new Random();
        int corect_answer = 0;
        int i =0;
        int answer = 0;
        
        Scanner input = new Scanner(System.in);
        
        System.out.println("I'm thinking of a number between 0 and 100 ...");       
        corect_answer = rd.nextInt(100);
        //System.out.println("so dc chon "+corect_answer);
        do {
            gues++;
            i++;
            System.out.println("You gues:");
            answer = input.nextInt();
            if (answer > corect_answer) {
                System.out.println("It's higher");
                
            } 
            else if(answer < corect_answer) {
                System.out.println("It's lower");
                
            } 
            else if(answer == corect_answer){
                System.out.println("You got it right in "+ i +" guesses");
                best.add(i);
            }
            } while (answer != corect_answer);
        System.out.println("Do you want to play again: ");
       
    }
    
    public void report(){
        Scanner sc = new Scanner(System.in);
        String selection;
        selection = sc.nextLine();
        
        switch(selection){
            case "yes" :
            case "Yes" :
            case "y" :
            case "Y" :
            case "YES" :
               play();
               game++;
               
               report();
               break;
            default:
                System.out.println("Overall Game: ");
                System.out.println("Total game: "+ game);
                System.out.println("Total guess: "+ gues);
                System.out.println("Guesses/game: "+((float)gues/(float)game));
                System.out.println("Best game: "+min(best));      
    }       
    }
    
    public static void main(String[] args) {
        LuckyNumber j = new LuckyNumber();       
        j.play();               
        j.report();
        
    } 
}
